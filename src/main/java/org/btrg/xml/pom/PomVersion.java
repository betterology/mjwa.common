package org.btrg.xml.pom;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

public class PomVersion {

	static Document getDocument(File pom) {
		URL url = null;
		try {
			url = pom.toURI().toURL();
		} catch (MalformedURLException e) {
			throw new RuntimeException();
		}
		SAXReader reader = new SAXReader();
		Document document = null;
		try {
			document = reader.read(url);
		} catch (DocumentException e) {
			throw new RuntimeException();
		}
		return document;
	}

	public static String get(File pom) {
		/*
		 * opens file, reads the version, returns it. That's all
		 */
		Document document = PomVersion.getDocument(pom);
		Node node = document
				.selectSingleNode("/*[name()='project']/*[name()='version']");
		return node.getText();
	}

	public static void set(File pom, String version) {
		/*
		 * opens file, sets the version, rewrites it
		 */
		Document document = PomVersion.getDocument(pom);
		Node node = document
				.selectSingleNode("/*[name()='project']/*[name()='version']");
		node.setText(version);
		writeFile(pom, document);

	}

	public static void setDependencyVersion(File pom, String groupId,
			String artifactId, String version) {
		boolean writeFile = false;
		/*
		 * opens file, searches all dependencies for matching groupid and
		 * artifactid. If finds it, sets the version, rewrites it
		 */

		Document document = PomVersion.getDocument(pom);
		List list = document
				.selectNodes("/*[name()='project']/*[name()='dependencies']/*[name()='dependency']/*[name()='groupId'][text()='"
						+ groupId + "']");
		for (Iterator<Element> iter = list.iterator(); iter.hasNext();) {
			boolean isElementWeWereLookingFor = false;
			Element groupIdNode = iter.next();
			Element dependencyNode = groupIdNode.getParent();

			for (int i = 0, size = dependencyNode.nodeCount(); i < size; i++) {
				Node node = dependencyNode.node(i);
				if (node instanceof Element) {
					Element inspectElement = (Element) node;
					if (inspectElement.getName().equals("artifactId")&&inspectElement.getText().equals(artifactId)) {
						isElementWeWereLookingFor = true;
					}
				}
			}
			if (isElementWeWereLookingFor) {
				for (int i = 0, size = dependencyNode.nodeCount(); i < size; i++) {
					Node node = dependencyNode.node(i);
					if (node instanceof Element) {
						Element versionElement = (Element) node;
						if (versionElement.getName().equals("version")) {
							versionElement.setText(version);
							writeFile = true;
							isElementWeWereLookingFor = false;//resetting for next one
						}
					}
				}
			}
		}
		if (writeFile) {
			writeFile(pom, document);
		}
	}

	static void writeFile(File pom, Document document) {
		OutputFormat format = OutputFormat.createPrettyPrint();
		XMLWriter writer;
		try {
			writer = new XMLWriter(new FileWriter(pom));
			writer.write(document);
			writer.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
