package org.btrg.gr.common

import org.btrg.gr.common.domain.DirControl;

/*
 * A primitive way of reading in a controls.txt file and finding which directories to continue reading
 */
class ActiveDirControls {
	public List<DirControl> get( ){
		List<DirControl> dirControl = new ArrayList<DirControl>();
		File file = new File(Constants.CONTROL_DIR+ '/'+ Constants.CONTROLS_FILE_NAME);
		file.eachLine {
			String line = it.toString()
			addIfOn(line, dirControl)
		}
		return dirControl;
	}

	void addIfOn(String line, List<DirControl>dirControls){
		if(!line.trim().startsWith('#')){
			if(line.toUpperCase().trim().endsWith("ON")){
				DirControl dirControl = new DirControl()
				dirControl.name = getName(line)
				dirControls.add(dirControl)
			}
		}
	}

	String getName(String line){
		String name = ''
		name = line.substring(0,line.indexOf(" "))
		return name;
	}
}
