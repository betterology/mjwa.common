package org.btrg.gr.common.domain

import java.util.List;

import org.btrg.active.GetProperty;
import org.btrg.gr.common.Constants;
import org.btrg.gr.module.Module;

class WorkspaceModulesAndProperties {
	Properties commonProperties
	Properties workspaceProperties
	List<Module> modules
	String workspaceWriteDirPath
	DirControl dirControl
	AntBuilder ant = new AntBuilder()
	List<String> existingModules

	public WorkspaceModulesAndProperties(DirControl dirControl, Properties commonProperties, Properties workspaceProperties, List<Module> modules, List<String> existingModules){
		this.dirControl = dirControl
		this.commonProperties = commonProperties
		this.workspaceProperties = workspaceProperties
		this.modules = modules
		this.existingModules = existingModules
		String pathToWriteNewModules = GetProperty.go(commonProperties, workspaceProperties, Constants.GENERATE_INTO_DIR)
	}
}
