package org.btrg.gr.common.domain

import java.io.File;
import java.util.List;
import java.util.Properties;

import org.btrg.active.GetProperty;
import org.btrg.file.ReadProperties;
import org.btrg.gr.common.ActiveDirControls;
import org.btrg.gr.common.Constants;
import org.btrg.gr.common.ExistingModules;
import org.btrg.gr.module.Module;
import org.btrg.gr.module.ReadModules;

class GetWorkspaceModulesAndProperties {
	public List<WorkspaceModulesAndProperties> get(){
		List<WorkspaceModulesAndProperties> workspaces = new ArrayList<WorkspaceModulesAndProperties>()
		Properties commonProperties = new ReadProperties().go(Constants.CONTROL_DIR+ '/'+ Constants.COMMON_PROPERTIES_FILE_NAME)
		AntBuilder ant = new AntBuilder()
		for(DirControl dirControl:new ActiveDirControls().get()){
			WorkspaceModulesAndProperties workspaceModulesAndProperties = getWorkspaceModulesAndProperties(dirControl, commonProperties, ant, )
			workspaces.add(workspaceModulesAndProperties)
		}
		return workspaces
	}

	WorkspaceModulesAndProperties getWorkspaceModulesAndProperties(DirControl dirControl, Properties commonProperties, AntBuilder ant){
		File dir = new File(Constants.CONTROL_DIR+ '/'+ dirControl.name);
		List<Module> modules
		List<String> existingModules
		Properties workspaceProperties
		workspaceProperties =new ReadProperties().go(dir.getAbsolutePath()+ '/'+ Constants.WORKSPACE_PROPERTIES_FILE_NAME);
		String pathToWriteNewModules = GetProperty.go(commonProperties, workspaceProperties, Constants.GENERATE_INTO_DIR)
		if(null!=pathToWriteNewModules){
			existingModules = ExistingModules.get(pathToWriteNewModules)
			modules = new ReadModules().go(dir)
		}
		WorkspaceModulesAndProperties workspaceModulesAndProperties = new WorkspaceModulesAndProperties(dirControl, commonProperties, workspaceProperties, modules, existingModules)
		return workspaceModulesAndProperties
	}
}
