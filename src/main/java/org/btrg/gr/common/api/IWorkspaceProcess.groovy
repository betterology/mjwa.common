package org.btrg.gr.common.api;

import org.btrg.gr.common.domain.WorkspaceModulesAndProperties;

public interface IWorkspaceProcess {
	public abstract void launch(
			WorkspaceModulesAndProperties workspaceModulesAndProperties);
}
