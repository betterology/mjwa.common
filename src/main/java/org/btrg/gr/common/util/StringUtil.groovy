package org.btrg.gr.common.util

import java.text.SimpleDateFormat;
import java.util.Date;

class StringUtil {


	public static String upLow(String var) {
		if (var == null | var.equals("")) {
			return " ";
		}
		String start = var.substring(0, 1);
		String finish = var.substring(1, var.length()).toLowerCase();
		return start.toUpperCase() + finish;
	}

	/**
	 * @param String
	 * @return String
	 */

	public static String upStart(String var) {
		if (var == null | var.equals("")) {
			return " ";
		}
		String start = var.substring(0, 1);
		String finish = var.substring(1, var.length());
		return start.toUpperCase() + finish;
	}


	public static String getMyClassName(String className) {
		String returnValue;
		int start = className.lastIndexOf(".") + 1;
		if (className.indexOf(".") > 0) {
			returnValue = className.substring(start, className.length());
		} else {
			returnValue = className;
		}
		return returnValue;
	}

	public static String getComponentFromJarName(String jarName) {
		if (jarName.contains("-")) {
			return jarName.substring(0,jarName.indexOf('-'));
		}else{
			return jarName;
		}
	}

	public static String yyMMdd(Date date) {
		return new SimpleDateFormat("yyMMdd").format(date);
	}

	public static String yyMMddHHmm(Date date) {
		return new SimpleDateFormat("yyMMddkkmm").format(date);
	}

	public static String getMyPackageName(String className) {
		String returnValue;
		int finish = className.lastIndexOf(".");
		if (className.indexOf(".") > 0) {
			returnValue = className.substring(0, finish);
		} else {
			returnValue = "has.no.real.package.sorry:" + className;
		}
		return returnValue;
	}

	public static String getTestName(String className, boolean exists) {
		if (exists) {
			return className + "Test" + yyMMdd(new Date());
		} else {
			return className + "Test";
		}
	}

	public static String upFirst(String var) {
		if (var == null | var.equals("")) {
			return " ";
		}
		String start = var.substring(0, 1);
		String finish = var.substring(1, var.length());
		return start.toUpperCase() + finish;
	}

	public static String getProjectName(String packageName, String workspacePackage) {
		if (packageName.startsWith(workspacePackage+'.')) {
			return packageName.substring(workspacePackage.length()+1, packageName.length());
		} else {
			return " error returning project name for "+ packageName + ' and '+workspacePackage;
		}
	}

	static boolean isLowerCaseCharachtersOnly(String string){
		boolean isLowerCaseCharachtersOnly = true
		int value
		for (int i = 0; i < string.length(); i++){
			value = (int)string.charAt(i)
			if (value>123||value<96){
				isLowerCaseCharachtersOnly = false
				break;
			}
		}

		return isLowerCaseCharachtersOnly
	}
}
