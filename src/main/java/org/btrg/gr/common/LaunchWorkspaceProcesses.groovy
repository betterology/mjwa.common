package org.btrg.gr.common

import org.btrg.file.ReadProperties;
import org.btrg.gr.common.api.IWorkspaceProcess;
import org.btrg.gr.common.domain.DirControl;
import org.btrg.gr.common.domain.GetWorkspaceModulesAndProperties;
import org.btrg.gr.common.domain.WorkspaceModulesAndProperties;
import org.btrg.gr.module.Module;


class LaunchWorkspaceProcesses {

	public void go(IWorkspaceProcess workspaceProcess){
		List<WorkspaceModulesAndProperties> workspaces = new GetWorkspaceModulesAndProperties().get()
		for(WorkspaceModulesAndProperties workspaceModulesAndProperties:workspaces ){
			workspaceProcess.launch(workspaceModulesAndProperties)
		}
	}
}
