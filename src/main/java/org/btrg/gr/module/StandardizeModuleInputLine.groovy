package org.btrg.gr.module

import org.btrg.gr.common.util.StringUtil;

class StandardizeModuleInputLine {
	//	need to put in a check here for hash value

	public static String get(String line){
		if(line.trim().startsWith('util|')){
			throw new RuntimeException('util is a reserved project and may be not be specified in the module.txt file')
		}
		if(isNotNull(line)&&isNotComment(line)){
			if(!startsWithValidHash(line)){
				throw new IllegalArgumentException('This is not a legal input line: \''+ line + '\' - possible problems are that first value must be lower case letters only and at least three charachters')
			}
			line = appendPipes(line)
			line = appendDescription(line)
		}
		else{
			return null
		}
		return line
	}

	static String appendPipes(String line){
		line = line.trim()
		int pipeCount = countPipes(line);
		if(pipeCount<6){
			int stillToAdd = 6-pipeCount
			for(int i=0;i<stillToAdd;i++){
				line = line+'|'
			}
		}
		return line
	}

	static String appendDescription(String line){
		line = line.trim()
		if(line.endsWith('|')){
			line = line+line.substring(0,line.indexOf('|'))+' description not prescribed in modules.txt file'
		}
		return line
	}
	static int countPipes(String line){
		int count = 0;
		for (int i = 0; i < line.length(); i++){
			if (line.charAt(i) == '|'){
				count++;
			}
		}
		return count
	}


	static boolean startsWithValidHash(String line){
		boolean returnValue = true
		String firstValue = line.substring(0, line.indexOf('|')).trim()
		if(firstValue.length()<3||!StringUtil.isLowerCaseCharachtersOnly(firstValue)){
			returnValue = false
		}
		return returnValue
	}

	static boolean isNotComment(String line){
		boolean returnValue = true
		if(null!=line&&line.trim().startsWith('#')){
			returnValue = false
		}
		return returnValue
	}

	static boolean isNotNull(String line){
		boolean returnValue = false
		if(null!=line&&line.trim().length()>0){
			returnValue = true
		}
		return returnValue
	}
}
