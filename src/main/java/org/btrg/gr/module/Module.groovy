package org.btrg.gr.module;

import org.btrg.gr.module.ModuleType;

class Module {
	def String directory
	def String name
	def String description
	def String hash
	def String version
	def String checkSum
	def int siz
	def boolean hashUsed
	def boolean exists
	def boolean service
	def ModuleType moduleType
}
