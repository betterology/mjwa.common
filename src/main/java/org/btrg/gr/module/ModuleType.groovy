package org.btrg.gr.module;

class ModuleType {
	def String name
	def String packaging //jar, war, yada
	def boolean apiOnly
	def boolean reserved
	def String style

}
