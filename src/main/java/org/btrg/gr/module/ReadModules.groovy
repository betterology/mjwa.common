package org.btrg.gr.module;

import java.beans.FeatureDescriptor;

import org.btrg.gr.common.Constants;


class ReadModules {
	/*
	 * Heads up, Module has a bunch of unused junk in it which is ignored in this class
	 */

	public List<Module> go(File dir){
		List<Module> modules = new ArrayList<Module>()
		File file = new File(dir, Constants.MODULES_FILE_NAME)
		file.eachLine {
			String line = it.toString()
			readProject(line, modules)
		}
		return modules
	}

	def readProject(String line,List<Module> modules){
		line = StandardizeModuleInputLine.get(line)
		String[] fields = null;
		if(null!=line){
			fields = line.split("[|]");
			if(fields.length > 6){
				if(!isNull(fields[0].trim())){
					Module module = new Module()
					module.setHash fields[0]
					module.setName isNull(fields[1]) ? fields[0].trim() : fields[1]
					module.setDirectory isNull(fields[2]) ? fields[0].trim() : fields[2]
					module.setVersion isNull(fields[3]) ? '0.0.1-SNAPSHOT' : fields[3]
					module.setDescription isNull(fields[6]) ? fields[6] +' description not prescribed in modules.txt file' : fields[6]
					//				module.setCheckSum project.@checkSum
					//				module.setExists new Boolean(project.@exists)
					//				module.setService new Boolean(project.@service)
					//				module.setSiz new Integer(project.@siz)
					//				module.setHashUsed new Boolean(project.@hashUsed)
					ModuleType moduleType = new ModuleType()
					moduleType.setPackaging isNull(fields[4]) ? 'jar' : fields[4]
					moduleType.setStyle isNull(fields[5])? 'standard' : fields[5]
					module.setModuleType moduleType
					modules.add module
				}
			}
		}
	}


	boolean isNull(String string){
		if(null!=string&&string.trim().length()>0){
			return false
		}
		return true
	}
}
