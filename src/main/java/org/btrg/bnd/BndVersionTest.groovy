package org.btrg.bnd;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Test;

class BndVersionTest {
	String filePath = "src/test/resources/org/btrg/bnd/bnd.bnd";
	private static final String INITIAL_VERSION = '0.0.110514'

	@Test
	public void testGet() {
		File bndFile = new File(filePath);
		assertEquals(INITIAL_VERSION, BndVersion.get(bndFile))
	}

	@Test
	public void testSet() {
		String qualifier = UUID.randomUUID().toString()
		File bndFile = new File(filePath)
		BndVersion.set(bndFile, '0.0.1-'+qualifier)
		assertEquals('0.0.1-'+qualifier, BndVersion.get(bndFile))
		BndVersion.set(bndFile, INITIAL_VERSION)
		assertEquals(INITIAL_VERSION, BndVersion.get(bndFile))
	}

}
