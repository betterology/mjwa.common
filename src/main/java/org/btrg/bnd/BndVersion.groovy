package org.btrg.bnd

class BndVersion {
	private static final String BUNDLE_VERSION = 'Bundle-Version:'
	public static String get(File bndFile){
		String version = ''
		String aValue = null
		bndFile.eachLine {
			String line = it.toString().trim()
			aValue = getVersion(line)
			if(null!=aValue){
				version=aValue;
			}
		}
		return version
	}

	public static void set(File bndFile, String version){
		StringWriter content = new StringWriter()
		bndFile.eachLine {
			String line = it.toString()
			line = setVersion(line, version)
			content.append(line+'\n')
		}
		bndFile.write(content.toString())
	}

	static String setVersion(String line, String version){
		if(line.startsWith(BUNDLE_VERSION)){
			line = BUNDLE_VERSION + ' '+ version.trim()
		}
		return line
	}

	static String getVersion(String line){
		String version = null
		if(	line.trim().startsWith(BUNDLE_VERSION)){
			version = line.substring(BUNDLE_VERSION.length()).trim()
		}
		return version;
	}
}
