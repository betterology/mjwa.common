package org.btrg.active

class GetProperty {
	public static String go(Properties commonProperties, Properties workspaceProperties, String key){
		if(workspaceProperties.containsKey(key)){
			return workspaceProperties.get(key)
		}else if (commonProperties.containsKey(key)){
			return commonProperties.get(key)
		}else{
			return null;
		}
	}
}
