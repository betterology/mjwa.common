package org.btrg.xml.pom;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;
import org.junit.Test;

public class PomVersionTest {
	String filePath = "src/test/resources/org/btrg/xml/pom/pom.xml";

	@Test
	public void testGetDocument() throws IOException {
		File file = new File(filePath);
		Document document = PomVersion.getDocument(file);
		assertNotNull(document);
		assertTrue(document.asXML().toString().contains("<bundle.location>org.btrg.mjwa/common/${project.version}</bundle.location>"));
	}

	@Test
	public void testGet() {
		File pom = new File(filePath);
		assertEquals("0.0.1-SNAPSHOT", PomVersion.get(pom));
	}

	@Test
	public void testSet() {
		File pom = new File(filePath);
		PomVersion.set(pom, "1.1.1");
		assertEquals("1.1.1", PomVersion.get(pom));
		PomVersion.set(pom, "0.0.1-SNAPSHOT");
	}

	@Test
	public void testSetDependencyVersion() {
		String uidqualifier = UUID.randomUUID().toString();
		File pom = new File(filePath);
		PomVersion.setDependencyVersion(pom, "org.btrg.mjwa", "foo", "0.0.1-"
				+ uidqualifier);
		Document document = PomVersion.getDocument(pom);
		assertTrue(document.asXML().toString().contains(uidqualifier));
		PomVersion.setDependencyVersion(pom, "org.btrg.mjwa", "foo",
				"0.0.1-SNAPSHOT");
	}

}
